function output=analyze(kind,truth,preds)	
% function output=analyze(kind,truth,preds)		
%
% Analyses the accuracy of a prediction
% Input:
% kind='acc' classification error
% kind='abs' absolute loss
% (other values of 'kind' will follow later)
% 


diff=abs(truth-preds);
n=length(truth);
switch kind
	case 'abs'
		% compute the absolute difference between truth and predictions
		output=sum(diff)/n;
	case 'acc' 
		output=length(find(~diff))/n;
end;

