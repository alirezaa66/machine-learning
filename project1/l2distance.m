function D=l2distance(x1,x2)
%tic
% function D=l2distance(x1,x2)
%	
% Computes the Euclidean distance matrix. 
% Syntax:
% D=l2distance(x1,x2)
% Input:
% x1: dxn data matrix with n vectors (columns) of dimensionality d
% x2: dxm data matrix with m vectors (columns) of dimensionality d
%
% Output:
% Matrix D of size nxm 
% D(i,j) is the Euclidean distance of x1(:,i) and x2(:,j)
%
% call with only one input:
% l2distance(x1)=l2distance(x1,x1)
%

if (nargin==1) % case when there is only one input (x1)
    G=innerproduct(x1);
    S=diag(G)*ones(1,size(x1,2));
	D=sqrt(S+S'-2*G);
else  % case when there are two inputs (x1,x2)
    n=size(x1,2);
    m=size(x2,2);
    G=innerproduct(x1,x2);
    if n>m
%       D=sqrt(Sx(1:n,1:m)-2*G+(Sz(:,1)*ones(1,n))');
%Sx=diag(innerproduct(x1))*ones(1,size(x1,2));
%              D=sqrt(Sx(1:n,1:m)-2*G+(diag(innerproduct(x2))*ones(1,n))');
               D=sqrt(diag(innerproduct(x1))*ones(1,m)-2*G+(diag(innerproduct(x2))*ones(1,n))');

    else
%       D=sqrt((Sz(1:m,1:n)-2*G'+(Sx(:,1)*ones(1,m))')');
%Sz=diag(innerproduct(x2))*ones(1,size(x2,2));
%             D=sqrt((Sz(1:m,1:n)-2*G'+(diag(innerproduct(x1))*ones(1,m))')');
              D=sqrt((diag(innerproduct(x2))*ones(1,n)-2*G'+(diag(innerproduct(x1))*ones(1,m))')');
    end
end;

%total_S_time=toc;
%total_S_time
%
