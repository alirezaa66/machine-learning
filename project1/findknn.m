function [indices,dists]=findknn(xTr,xTe,k);
% function [indices,dists]=findknn(xTr,xTe,k);
%
% Finds the k nearest neighbors of xTe in xTr.
%
% Input:
% xTr = dxn input matrix with n column-vectors of dimensionality d
% xTe = dxm input matrix with n column-vectors of dimensionality d
% k = number of nearest neighbors to be found
% 
% Output:
% indices = kxm matrix of the where indices(i,j) is the i^th nn of xTe(:,j)
% i=(1,2,...,k) and j=
% dists = Euclidean distances to the respective nearest neighbors
%
if k>size(xTr,2)
    k=size(xTr,2);
    disp('k was set to the number of training data points');
end
[m ind]=sort(l2distance(xTr,xTe));
indices=ind(1:k,:);
dists=m(1:k,:);
%%	%
	
