function [loss,gradient,preds]=hinge(w,xTr,yTr,lambda)
% function w=ridge(xTr,yTr,lambda)
%
% INPUT:
% xTr dxn matrix (each column is an input vector)
% yTr 1xn matrix (each entry is a label)
% lambda regression constant
% w weight vector (default w=0)
%
% OUTPUTS:
%
% loss = the total loss obtained with w on xTr and yTr
% gradient = the gradient at w
%

[d,n]=size(xTr);
loss = lambda * sum(w.^2);
gradient = w*0;
for i = 1:n
    gradient = gradient - heaviside(1-yTr(i)*w'*xTr(:,i)) * yTr(i)*xTr(:,i);
    loss = loss + max(1-yTr(i)*w'*xTr(:,i),0);
end
gradient = gradient + 2*lambda*w;
preds = w' * xTr;
preds = preds > 0;
preds = preds*2 - 1;