1. Use "git svn clone https://shell.cec.wustl.edu:8443/cse517a_sp15/svn/alireza/" to download repo from Kilian's server

2. Add Bitbucket with "git remote add origin https://alirezaa66@bitbucket.org/alirezaa66/machine-learning.git"

3. To download changes from Bitbucket, run: "git pull origin master"
4. To upload changes to Bitbucket, run: "git push origin master"
5. To upload changes to Kilian's server, run: "git svn dcommit"
6. To download changes from Kilian, run: "git svn rebase"

