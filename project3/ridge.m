function [loss,gradient]=ridge(w,xTr,yTr,lambda)
% function w=ridge(xTr,yTr,lambda)
%
% INPUT:
% w weight vector (default w=0)
% xTr dxn matrix (each column is an input vector)
% yTr 1xn matrix (each entry is a label)
% lambda regression constant
%
% OUTPUTS:
% loss = the total loss obtained with w on xTr and yTr
% gradient = the gradient at w
%

%tic
if nargin < 4
    lambda = 0.2;
end

%[d,n] = size(xTr);
X = (full(xTr))';
Y = (full(yTr))';

%loss = (w'*X'-Y')*(X*w-Y)+lambda*(w'*w);
loss = sum((X*w-Y).^2) + lambda*sum(w.^2);
%%%%w = (X'*X+lambda*I)^-1*X'*Y; % From weighted ridge in HW3
gradient = 2*X'*(X*w-Y) + 2*lambda*w;


%gradient = w*0;
%loss = 0;
%for i = 1:n
%    xi = full(xTr(:,i)); % The original xTr is a sparse matrix
%    yi = yTr(i);
%    gradient = gradient + 2*(xi*(w'*xi-yi)+lambda*w);
%    loss = loss + (w'*xi-yi)^2 + lambda*w'*w;
%end

%total_time = toc;
%total_time