%function [preds]=ridge(w,xTe)
function [preds]=linclassify(w,xTe)

% function w=ridge(xTr,yTr,lambda)
%
% INPUT:
% w weight vector (default w=0) dx1
% xTe dxn matrix (each column is an input vector)
%
% OUTPUTS:
% 
% preds predictions
% 

preds = w'*xTe;



