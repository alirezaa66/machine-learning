function err=looreg(xTr,yTr);
%	function err=looreg(xTr,yTr);
%
%   INPUT:
%   xTr = dxn matrix of n input vectors 
%	yTr = 1xn vector of n target values
%
%   OUTPUT:
%   err = root mean-squared error of linear regression with LOOCV
%
[d, n] = size(xTr);
X = xTr';
Y = yTr';
H = X*(X'*X)^-1*X';
Y_hat = H*Y;

%regular_error = 1-sum(Y==round(Y_hat))/length(Y);
regular_RMSE = sum((Y-Y_hat).^2);
regular_RMSE = sqrt(regular_RMSE/n);
preds = [Y_hat Y];
regular_RMSE
%preds

LOOCV = 0;
for k = 1:n
    LOOCV = LOOCV + ((Y(k)-Y_hat(k))/(1-H(k,k)))^2;
end
err = sqrt(LOOCV/n);



