function [w]=grdescent(func,w0,stepsize,maxiter,tolerance)
% function [w]=grdescent(func,w0,stepsize,maxiter,tolerance)
%
% INPUT:
% func function to minimize
% w0 = initial weight vector 
% stepsize = initial gradient descent stepsize 
% tolerance = if norm(gradient)<tolerance, it quits
%
% OUTPUTS:
% 
% w = final weight vector
%

w = w0;
if nargin<5,tolerance=1e-02;end;
for i = 1:maxiter
	[l1,g1] = func(w);
	if norm(g1) < tolerance
		disp('Tolerance Satisfied. Iterations:')
		disp(i);
		break
	end
	
	w2 = w - stepsize * g1;
	[l2,~] = func(w2);
	
	if l1 > l2
			stepsize = 1.01 * stepsize;
			w = w2;
	else
			stepsize = stepsize * 0.5;
	end
end



%Some results:
%with the following input it took 556 iterations to minize ridge to norm<0.01
% f=@(w) ridge(w,xTr,yTr,0.1);
% [w]=grdescent(f,zeros(size(xTr,1),1),0.045,1000,1e-2)