function [xTr,xTe,u,m]=preprocess(xTr,xTe);
% function [xTr,xTe,u,m]=preprocess(xTr,xTe);
%
% Preproces the data to make the training features have zero-mean and
% standard-deviation 1
%
% output:
% xTr - pre-processed training data 
% xTe - pre-processed testing data
% 
% u,m - any other data should be pre-processed by x-> u*(x-m)
%

%%% variance of each feature = 1
m = mean(xTr,2);
u = diag(1./std(xTr'));
xTr = u*bsxfun(@minus, xTr, m);
xTe = u*bsxfun(@minus, xTe, m);


%%% With PCA
% m = mean(xTr,2);
% xTr = bsxfun(@minus, xTr, m);
% xTe = bsxfun(@minus, xTe, m);
% 
% [U,S,V] = svd(xTr');
% 
% xTr = pinv(sqrt(S))*V'*xTr';
% xTe = pcacov(xTe');
% u = diag(1./std(xTr'));
% xTr = u*xTr;
% xTe = u*xTe;

%u = diag(1./std(xTr'));
%xTr = u*(xTr-m*ones(1,size(xTr,2)));
%xTe = u*(xTe-m*ones(1,size(xTe,2)));
