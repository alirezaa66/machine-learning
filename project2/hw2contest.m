function testfunc=hw2contest(xTr,yTr)
%% Modify code below here

% Normalize the input data
d = size(xTr,1);
n = size(xTr,2);
m1 = min(xTr,[],2);
m2 = max(xTr,[],2);
%xTr = ((xTr - repmat(m1,1,n)) ./ repmat(m2-m1,1,n));

doExpand = true;

% do training
%T = id3tree(xTr,yTr);
F = forest(xTr,yTr,100,doExpand);
%BDT = boosttree(xTr,yTr,30,4);

% define the testing function
%testfunc = @(xTe,yTe) evaltree(T,xTe);
testfunc = @(xTe,yTe) evalforest(F,xTe,doExpand);
%testfunc = @(xTe,yTe) evalboost(BDT,xTe);

% You can evaluate your code with some validation data xTv,yTv
% calling:
% valerror=testfunc(xTv,yTv);
%
