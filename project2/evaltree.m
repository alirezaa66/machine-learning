function [ypredict]=evaltree(T,xTe)
% function [ypredict]=evaltree(T,xTe);
%
% input: 
% T0  | tree structure
% xTe | Test data (dxn matrix)
%
% output: 
%
% ypredict : predictions of labels for xTe
%



y = zeros(1,size(xTe,2));
for i=1:length(xTe)
    pointer = 1;
    while T(2,pointer) > 0
        feature = T(2,pointer);
        cut = T(3,pointer);
        if xTe(feature,i) <= cut
            % Go down the left branch
            pointer = T(4,pointer);
        else
            % Go down the right branch
            pointer = T(5,pointer);
        end
    end
    y(i) = T(1,pointer);
end

ypredict=y;
