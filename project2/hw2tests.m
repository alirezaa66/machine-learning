function [r,s]=hw2tests()
% function [r,s]=hw2tests()
%
% Tests the functions from homework assignment 1
% Please make sure that the error statements are instructive. 
%
% Output: 
% r=0 number of tests that broke
% s= statement describing the failed test (s={} if all succeed)
%


% Put in any seed below
rand('seed',31415926535);
% initial outputs
r=0;
s={};


% Example:
% Check if xor data is split correctly. 

x=[  0     1     0     1
     1     0     0     1];
y =[  2     2     1     1];
[f,c]=entropysplit(x,y);
if c<=0 || c>1,			% if test failed ...
	r=r+1;				% set output conditions and exit
	s{length(s)+1}='First XOR split not correct.';
end;


x=[  0     0
     1     0];
y =[  2    1];
[f,c]=entropysplit(x,y);
if f~=2,			% if test failed ...
	r=r+1;				% set output conditions and exit
	s{length(s)+1}='In the XOR 2nd split, it splits on the wrong feature.';
end;

if f==1 & (c<=1 | c>0),			% if test failed ...
	r=r+1;				% set output conditions and exit
	s{length(s)+1}='2nd XOR split feature is correct, but the cut value is wrong.';
end;


% Purely functional tests for the other custom functions.
% Tree
load('heart.mat');
T = id3tree(xTr,yTr);
Tpred = evaltree(T,xTe);
if size(T,2)<1
    r = r+1;
    s{length(s)+1} = 'id3tree returned empty tree';
end
if analyze('acc',yTe,Tpred) < 0.5
    r = r+1;
    s{length(s)+1} = 'id3tree had accuracy under 50%';
end

% Forest
F = forest(xTr,yTr,50);
Fpred = evalforest(F,xTe);
if size(F{1},2)<1
    r = r+1;
    s{length(s)+1} = 'forest returned empty tree';
end
if analyze('acc',yTe,Fpred) < 0.5
    r = r+1;
    s{length(s)+1} = 'forest had accuracy under 50%';
end

% Boost
B = boosttree(xTr,yTr,15,4);
Bpred = evalforest(B,xTe);
if size(F{1},2)<1
    r = r+1;
    s{length(s)+1} = 'boost returned empty tree';
end
if analyze('acc',yTe,Bpred) < 0.5
    r = r+1;
    s{length(s)+1} = 'boost had accuracy under 50%';
end

% Prune
P = prunetree(T,xTe,yTe);
if sum(P(1,:)>0) >= sum(T(1,:)>0)
    % This test assumes that unused nodes are "zeroed out" when they are
    % prunned from the tree.
    r = r+1;
    s{length(s)+1} = 'prunetree did not prune correctly';
end

