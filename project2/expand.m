function [ xEx ] = expand( xTr )
%expand(xTr)
% Applies a basis expansion to xTr.

d = size(xTr,1);
n = size(xTr,2);

% Apply a basis expansion
xEx = zeros(d*2, n);
xEx(1:d,:) = xTr;
xEx((d+1):(2*d),:) = exp(xTr);

end

