function hbar=computehbar(xTe,treedepth)
% function [hbar]=computehbar(xTe,treedepth);
% 
% computes the expected prediction of the average classifier (hbar)
% for data set xTe. 
% The of size Nsmall drawn from toydata.m with OFFSET 
% with id3trees with maxdepth "treedepth"
%
% The "infinite" number of models is estimated as an average over NMODELS. 
%
% INPUT:
% xTe       | dxn matrix, of n column-wise input vectors (each d-dimensional)
% treedepth | max treedepth of the decision tree models
%

global Nsmall NMODELS OFFSET;
[~,n]=size(xTe);
hbar=zeros(1,n);

%Nsmall=500; % how big is the training set size N
%NMODELS=25; % how many models do you want to average over
%OFFSET=2;   % offset between the two gaussians

for j=1:NMODELS
[x,y]=toydata(OFFSET,Nsmall);
T=id3tree(x,y,treedepth);
hbar=hbar+evaltree(T,xTe);
end;
hbar=hbar./NMODELS;
%hbar=round(hbar);

