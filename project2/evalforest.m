function preds=evalforest(F,xTe,doExpand)
% function preds=evalforest(F,xTe);
%
% Evaluates a random forest on a test set xTe.  
%
% input: 
% F   | Forest of decision trees
% xTe | matrix of m input vectors (matrix size dxm)
%
% output: 
%
% preds | predictions of labels for xTe
%

if nargin<3
    doExpand = false;
end

function [ xEx ] = expand( xTr )
    d = size(xTr,1); n = size(xTr,2);
    % Apply a basis expansion
    xEx = zeros(d*2, n);
    xEx(1:d,:) = xTr;
    xEx((d+1):(2*d),:) = xTr.^2;
end
if doExpand
    xTe = expand(xTe);
end

ypredict=zeros(length(F),size(xTe,2));
for i=1:length(F)
    [ypredict(i,:)]=evaltree(F{i},xTe);
end
preds=mean(ypredict,1);
preds=round(preds);

end

