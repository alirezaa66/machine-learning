function F=forest(x,y,nt,doExpand)
% function F=forest(x,y,nt)
%
% INPUT: 
% x | input vectors dxn
% y | input labels 1xn
%
% OUTPUT:
% F | Forest
%

if nargin<4
    doExpand = false;
end

function [ xEx ] = expand( xTr )
    d = size(xTr,1); n = size(xTr,2);
    % Apply a basis expansion
    xEx = zeros(d*2, n);
    xEx(1:d,:) = xTr;
    xEx((d+1):(2*d),:) = xTr.^2;
end
if doExpand
    x = expand(x);
end

% We get better accuracy using 70% of features rather than sqrt(n) features.
number_of_features = ceil(0.70*size(x,1));
F={}; % An array cell members of which are the T matrices (trees)
for i=1:nt
    selected_points = randi(size(x,2),1,size(x,2));
    selected_features = randsample(size(x,1),number_of_features);
    T = id3tree(x(selected_features,selected_points),y(selected_points));
    T = prunetree(T,x,y);
    for j=1:size(T,2)
        if T(2,j)>0
            T(2,j)=selected_features(T(2,j)); % The number of the feature to cut is from the original numbers of feature in the main matrix x so that we can use this tree on the original dataset
        end
    end
    F{i}=T;
end

end

