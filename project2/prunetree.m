function Tt=prunetree(T,xTe,y)
% function T=prunetree(T,xTe,y)
%
% Prunes a tree to minimal size such that performance on data xTe,y does not
% suffer.
%
% Input:
% T = tree
% x = validation data x (dxn matrix)
% y = labels (1xn matrix)
%
% Output:
% T = pruned tree 
%

function err = prune_recurse(q, x, y)
    % Extract the information from the struct
    t = T(:,q);
    prediction = t(1);
    feature = t(2);
    cut = t(3);
    li = t(4);
    ri = t(5);
    
    % Compute the error at this node
    if isempty(y)
        err = 1;
    else
        err = 1 - sum(y==prediction)/length(y);
    end
    
    % Return now if we are a leaf (nothing to do)
    if feature==0
        return;
    end
    
    % Recurse down the tree
    idxL = x(feature,:) <= cut;
    idxR = x(feature,:) > cut;
    errL = prune_recurse(li, x(:,idxL), y(idxL));
    errR = prune_recurse(ri, x(:,idxR), y(idxR));
    
    % Begin the attempt to prune.
    % Do not prune if at least of our children is a subtree
    if (T(2,li)>0) || (T(2,ri)>0)
        return;
    end
    
    % Can we prune without increasing validation error?
    if (errL+sqrt(eps) >= err) && (errR+sqrt(eps) >= err)
        % We can prune!!
        T(:,li) = zeros(6,1);
        T(:,ri) = zeros(6,1);
        T(2:5,q) = zeros(4,1);
    end
end

prune_recurse(1, xTe, y);

%{
for i=1:size(T,2)
    x_array=stack_x{i};
    y_array=stack_y{i};
    if T(2,i)>0
        left_index=find(x_array(T(2,i),:)<=T(3,i));
        right_index=find(x_array(T(2,i),:)>T(3,i));
    end
    if T(4,i)>0
        stack_x{T(4,i)}=x_array(:,left_index);
        stack_y{T(4,i)}=y_array(left_index);
    end
    if T(5,i)>0
        stack_x{T(5,i)}=x_array(:,right_index);
        stack_y{T(5,i)}=y_array(right_index);
    end
end

for i=size(T,2):-1:2;
    x_array=stack_x{i};
    if size(x_array,2)==0
        
        left_or_right=find(T(:,T(6,i))==i);
        T(left_or_right,T(6,i))=0;
        if (T(4,T(6,i))==0)&&(T(5,T(6,i))==0)
            T(2:5,T(6,i))=0;
        end
        if i+1<=size(T,2)
            for j=i+1:size(T,2)
                if T(4,T(6,j))>0
                    T(4,T(6,j))=T(4,T(6,j))-1;
                end
                if T(5,T(6,j))>0
                    T(5,T(6,j))=T(5,T(6,j))-1;
                end
                if T(6,j)>=i
                    T(6,j)=T(6,j)-1;
                end
            end
        end
        
        T(:,i)=[];
    end
end


for i=size(T,2):-1:2;
    if T(4,i)*T(5,i)==0
        if T(4,i)+T(5,i)~=0
            ii=T(4,i)+T(5,i);
            T(2:5,i)=0;
        if ii+1<=size(T,2)
            for j=ii+1:size(T,2)
                if T(4,T(6,j))>0
                    T(4,T(6,j))=T(4,T(6,j))-1;
                end
                if T(5,T(6,j))>0
                    T(5,T(6,j))=T(5,T(6,j))-1;
                end
                if T(6,j)>=i
                    T(6,j)=T(6,j)-1;
                end
            end
        end
        T(:,ii)=[];
        end
    end
end
%}

Tt = T;

end
