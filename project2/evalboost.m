function preds=evalboost(BDT,xTe)
% function preds=evalboost(BDT,xTe);
%
% Evaluates a boosted decision tree on a test set xTe.  
%
% input: 
% BDT | Boosted Decision Trees
% xTe | matrix of m input vectors (matrix size dxm)
%
% output: 
%
% preds | predictions of labels for xTe
%

%H=zeros(size(BDT,1),size(xTe,2));
%preds=zeros(1,size(xTe,2));

function [ xEx ] = expand( xTr )
    d = size(xTr,1); n = size(xTr,2);
    % Apply a basis expansion
    xEx = zeros(d*2, n);
    xEx(1:d,:) = xTr;
    xEx((d+1):(2*d),:) = exp(xTr);
end
%xTe = expand(xTe);

H = zeros(1,size(xTe,2));
sum_alpha = 0;
for i = 1:size(BDT,1)
    H = H+BDT{i,2}*evaltree(BDT{i,1},xTe);
    sum_alpha = sum_alpha + BDT{i,2};
end
preds = round(H/sum_alpha);

end
