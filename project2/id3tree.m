function T=id3tree(xTr,yTr,maxdepth,weights)
% function T=id3tree(xTr,yTr,maxdepth,weights)
%
% The maximum tree depth is defined by "maxdepth" (maxdepth=2 means one split). 
% Each example can be weighted with "weights". 
%
% Builds an id3 tree
%
% Input:
% xTr | dxn input matrix with n column-vectors of dimensionality d
% xTe | dxm input matrix with n column-vectors of dimensionality d
% maxdepth = maximum tree depth
% weights = 1xn vector where weights(i) is the weight of example i
% 
% Output:
% T = decision tree 
%

d = size(xTr,1);
n = size(xTr,2);
if nargin<3
    maxdepth = 0;
end
if nargin<4
    weights = ones(1,n)./n;
end





% Use one stack for all our data structs.
%  struct{1} = x values
%  struct{2} = y values
%  struct{3} = weight vector
%  struct{4} = parent node id
%  struct{5} = left child flag (true=left child, false=right child)
%  struct{6} = depth up to this point
stack = {};

% Initialize variables
stack{1} = {xTr, yTr, weights, 0, false, 0};
T = [];
q = 0;
si = 1;

% Perform the recursion
while si > 0
    % Pop an item off the stack
    struct = stack{si};
    si = si - 1;
    
    % Increment the table pointer.
    q = q + 1;
    
    % Extract the information from the struct
    x = struct{1};
    y = struct{2};
    w = struct{3};
    parent = struct{4};
    leftflag = struct{5};
    depth = struct{6};
    
    % Save information into T
    T(1,q) = mode(y); % default prediction: most common label
    T(2,q) = 0;
    T(3,q) = 0;
    T(4,q) = 0;
    T(5,q) = 0;
    T(6,q) = parent;
    
    % Save in the child indices
    if parent > 0
        if leftflag
            T(4,parent) = q;
        else
            T(5,parent) = q;
        end
    end
    
    % Quit if there is no entropy in this split
    if length(unique(y)) <= 1
        continue;
    end
    
    % Perform the entropy split
    [feature,cut,~] = entropysplit(x, y, w);
    
    % Quit if feature==0
    if (feature==0), continue; end
    
    if (maxdepth==0) || (depth<maxdepth)
        % We found a split.  Prepare the left and right children.
        idxL = x(feature,:) <= cut;
        idxR = x(feature,:) > cut;
        structL = {x(:,idxL), y(idxL), w(idxL), q, true, depth+1};
        structR = {x(:,idxR), y(idxR), w(idxR), q, false, depth+1};
        
        % Do NOT recurse if the length of left or right is zero
        if (sum(idxL)==0) || (sum(idxR)==0), continue; end
        
        % Push them onto the stack.
        stack{si+1} = structL;
        stack{si+2} = structR;
        si = si + 2;
        
        % Save into the table
        T(2,q) = feature;
        T(3,q) = cut;
    end
end

%{
i=size(T,2);
while i>1
    if (T(4,i)==0)&&(T(5,i)==0)
        %if (T(6,i)==T(6,i-1))&&(T(1,i)==T(1,i-1))
        if T(:,i)==T(:,i-1)
            T(2:5,T(6,i))=0;
            if i+2<=size(T,2)
            for j=i+1:size(T,2)
                T(4,T(6,j))=T(4,T(6,j))-1;
                T(5,T(6,j))=T(5,T(6,j))-1;
                if T(6,j)>=i
                    T(6,j)=T(6,j)-2;
                end
            end
            end
            T(:,i)=[];
            T(:,i-1)=[];
            i=i-2;
            
        else
            i=i-1;
        end
    else
        i=i-1;
    end
end
%}
