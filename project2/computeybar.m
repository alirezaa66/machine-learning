function ybar=computeybar(xTe)
% function [ybar]=ybar(xTe);
% 
% computes the expected label 'ybar' for a set of inputs x
% generated from two standard Normal distributions (one offset by OFFSET in
% both dimensions.)
%
% INPUT:
% xTe | a dxn matrix of column input vectors
% 
% OUTPUT:
% ybar | a 1xn vector of the expected label ybare(x)
%

global OFFSET;

mu1 = [0,0];
mu2 = [OFFSET,OFFSET];
Sigma = eye(2);

% Compute the normal distribution values
px_y1 = mvnpdf(xTe', mu1, Sigma);
px_y2 = mvnpdf(xTe', mu2, Sigma);

% Apply Bayes' rule
py1 = 0.5;
py2 = 0.5;
px = px_y1.*py1 + px_y2.*py2;
py1_x = px_y1.*py1./px;
py2_x = px_y2.*py2./px;

% Compute ybar
ybar = 1*py1_x + 2*py2_x;
ybar = ybar';
