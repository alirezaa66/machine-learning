function BDT=boosttree(x,y,nt,maxdepth)
% function BDT=boosttree(x,y,nt,maxdepth)
%
% Learns a boosted decision tree on data x with labels y.
% It performs at most nt boosting iterations. Each decision tree has maximum depth "maxdepth".
%
% INPUT: 
% x  | input vectors dxn
% y  | input labels 1xn
% nt | number of trees (default = 100)
% maxdepth | depth of each tree (default = 3)
%
% OUTPUT:
% BDT | Boosted DTree
%

function [ xEx ] = expand( xTr )
    d = size(xTr,1); n = size(xTr,2);
    % Apply a basis expansion
    xEx = zeros(d*2, n);
    xEx(1:d,:) = xTr;
    xEx((d+1):(2*d),:) = exp(xTr);
end
%x = expand(x);

BDT = {}; % a cell array with the trees stored in the first column and their coefficient in the second
weights = ones(1,length(y))/length(y);

for i = 1:nt
    
    % Compute the ID3 tree with the current weights
    T = id3tree(x,y,maxdepth,weights);
    
    % Evaluate the tree (current guess)
    h = evaltree(T,x);

    % Calculate epsilon and weights (error of the current guess, h)
    err = y~=h;
    err = (2*err-1);
    err = exp(err);
    diff = abs(y-h);
    error_index = diff>0;
    epsilon_weight = weights(error_index); %length(epsilon_number)/length(y);
    epsilon = sum(epsilon_weight);
    
    % Calcualate Alpha
    alpha = 0.5*log((1-epsilon)/epsilon);
    
    % Update weights
    weights = exp(weights).*err*exp(alpha);
    weights = weights./sum(weights);
    
    % Evaluate H
    H = 0;
    sum_alpha = 0;
    for j = 1:i-1
        H = H+BDT{j,2}*evaltree(BDT{j,1},x);
        sum_alpha = sum_alpha + BDT{j,2};
    end
    H = (H + alpha*h)/(sum_alpha+alpha);

    % Chekck to see if h is doing better than a random classifier
    if epsilon>0.5
        disp('This calculation might diverge since epsilon>0.5');
        break;
    end

    % Returning outputs
    BDT{i,1} = T;
    BDT{i,2} = alpha;
end

end
